L.TileLayer.BetterWMS = L.TileLayer.WMS.extend({

    onAdd: function(map) {
        // Triggered when the layer is added to a map.
        //   Register a click listener, then do all the upstream WMS things
        L.TileLayer.WMS.prototype.onAdd.call(this, map);
        map.on('click', this.getFeatureInfo, this);
        //q500.bringToFront();
        q100.bringToFront();
        q20.bringToFront();
        q5.bringToFront();
        orp.bringToFront();
        obce.bringToFront();
        mestcasti.bringToFront();
        hladinomery.bringToFront();
        srazkomery.bringToFront();
    },

    onRemove: function(map) {
        // Triggered when the layer is removed from a map.
        //   Unregister a click listener, then do all the upstream WMS things
        L.TileLayer.WMS.prototype.onRemove.call(this, map);
        map.off('click', this.getFeatureInfo, this);
    },

    getFeatureInfo: function(evt) {
        // Make an AJAX request to the server and hope for the best
        var url = this.getFeatureInfoUrl(evt.latlng),
            showResults = L.Util.bind(this.showGetFeatureInfo, this);
        $.ajax({
            url: url,
            success: function(data, status, xhr) {
                var err = typeof data === 'object' ? null : data;
                showResults(err, evt.latlng, data);
            },
            error: function(xhr, status, error) {
                showResults(error);
            }
        });
    },

    getFeatureInfoUrl: function(latlng) {
        // Construct a GetFeatureInfo request URL given a point
        var point = this._map.latLngToContainerPoint(latlng, this._map.getZoom()),
            size = this._map.getSize(),

            params = {
                request: 'GetFeatureInfo',
                service: 'WMS',
                srs: 'EPSG:4326',
                styles: this.wmsParams.styles,
                transparent: this.wmsParams.transparent,
                version: this.wmsParams.version,
                format: this.wmsParams.format,
                bbox: this._map.getBounds().toBBoxString(),
                height: size.y,
                width: size.x,
                layers: this.wmsParams.layers,
                query_layers: this.wmsParams.layers,
                info_format: 'application/json',


            };

        if (this.wmsParams.CQL_FILTER) {
            params.CQL_FILTER = this.wmsParams.CQL_FILTER;
        }

        params[params.version === '1.3.0' ? 'i' : 'x'] = point.x;
        params[params.version === '1.3.0' ? 'j' : 'y'] = point.y;

        return this._url + L.Util.getParamString(params, this._url, true);
    },

    showGetFeatureInfo: function(err, latlng, content) {
        if (err) {
            console.log('err');
            return;
        } // do nothing if there's an error

        // Otherwise show the content in a popup, or something.
        if (String(content) != '[object XMLDocument]' && content.features.length > 0) {
            content = String(content.features[0].properties.popis);
            var findpar = '<p>|<p align="left">';
            var findendpar = '</p>|<br><br>|<br /><br />|<br>&nbsp;<br>';
            var re = new RegExp(findpar, 'g');
            var reend = new RegExp(findendpar, 'g');
            content = content.replace(re, '<br>');
            content = content.replace(reend, '');
        } else {
            //console.log('Not feature here')
        };

        if (content.length > 80) {
            L.popup({
                    maxWidth: 800
                })
                .setLatLng(latlng)
                .setContent(content)
                .openOn(this._map);
        }
    }
});

L.tileLayer.betterWms = function(url, options) {
    return new L.TileLayer.BetterWMS(url, options);
};

selectparams = {};
var mbAttr = 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
    '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
    'Imagery © <a href="http://mapbox.com">Mapbox</a>',
    mbUrl = 'https://edpp.cz/geoserver/gwc/service/wmts?layer=osm%3Aenvi_osm_gray&style=&tilematrixset=3857&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image%2Fpng&TileMatrix=3857:{z}&TileCol={x}&TileRow={y}',
    mpCZ = 'https://edpp.cz/geoserver/gwc/service/wmts?layer=osm%3Aenvi_osm&style=&tilematrixset=3857&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image%2Fpng&TileMatrix=3857:{z}&TileCol={x}&TileRow={y}',
    cuzk_orft = '',
    mpCZatr = '<a href="https://www.envipartner.cz" target="_blank">všechna práva vyhrazena ENVIPARTNER, s.r.o.</a>';

var grayscale = L.tileLayer(mbUrl, {
        id: 'mapbox.light',
        attribution: mbAttr
    }),
    mapy_cz = L.tileLayer(mpCZ, {
        id: 'envi_osm_base',
        attribution: mpCZatr
    });

var ortofoto = L.tileLayer.betterWms('https://geoportal.cuzk.cz/WMS_ORTOFOTO_PUB/WMService.aspx', {
    layers: 'GR_ORTFOTORGB',
    format: 'image/png',
    tiled: 'true',
    attribution: '© ČÚZK',
});

var q5 = L.tileLayer('https://edpp.cz/geoserver/gwc/service/wmts?layer=zu:cr_zaplavove_5_let&style=zu_q5&tilematrixset=3857&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image/png&TileMatrix=3857:{z}&TileCol={x}&TileRow={y}', {
    transparent: 'true'
});
var q20 = L.tileLayer('https://edpp.cz/geoserver/gwc/service/wmts?layer=zu:cr_zaplavove_20_let&style=zu_q20&tilematrixset=3857&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image/png&TileMatrix=3857:{z}&TileCol={x}&TileRow={y}', {
    transparent: 'true'
});
var q100 = L.tileLayer('https://edpp.cz/geoserver/gwc/service/wmts?layer=zu:cr_zaplavove_100_let&style=zu_q100&tilematrixset=3857&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image/png&TileMatrix=3857:{z}&TileCol={x}&TileRow={y}', {
    transparent: 'true'
});

/*var q500 = L.tileLayer.betterWms('https://www.edpp.cz/geoserver/CR_zaplavove/wms?', {
    layers: 'CR_zaplavove:CR_zaplavove_500_let_wgs',
    format: 'image/png',
    tiled: 'true',
    transparent: 'true'
});*/

var hladinomery = L.tileLayer.betterWms('https://www.edpp.cz/geoserver/lvs/wms', {
    layers: 'lvs:edpp-hladinomery',
    format: 'image/png',
    tiled: 'true',
    transparent: 'true'
});
var srazkomery = L.tileLayer.betterWms('https://www.edpp.cz/geoserver/lvs/wms?', {
    layers: 'lvs:edpp-srazkomery',
    format: 'image/png',
    tiled: 'true',
    transparent: 'true'
});
var obce = L.tileLayer.betterWms('https://www.edpp.cz/geoserver/admin_cleneni/wms?', {
    layers: 'admin_cleneni:edpp-obce',
    format: 'image/png',
    tiled: 'true',
    transparent: 'true'
});
var orp = L.tileLayer.betterWms('https://www.edpp.cz/geoserver/admin_cleneni/wms?', {
    layers: 'admin_cleneni:edpp-orp',
    format: 'image/png',
    tiled: 'true',
    transparent: 'true'
});

var mestcasti = L.tileLayer.betterWms('https://www.edpp.cz/geoserver/admin_cleneni/wms?', {
    layers: 'admin_cleneni:edpp-mestske-casti',
    format: 'image/png',
    tiled: 'true',
    transparent: 'true'
});

var layers = {
    '<img class="layer_label" src="https://edpp.cz/public/images/new-edpp-viewer/icons/hladinomer-icon.png" alt="hladinomery icon" height="16" width="16"><spam>Hladinoměry</spam>': hladinomery,
    '<img class="layer_label" src="https://edpp.cz/public/images/new-edpp-viewer/icons/srazkomer-icon.png" alt="srazkomer-icon icon" height="16" width="16"><spam>Srážkoměry</spam>': srazkomery,
    '<img class="layer_label" src="https://edpp.cz/public/images/new-edpp-viewer/icons/q5-icon.png" alt="q5-icon.png icon" height="16" width="16"><spam>Záplavové území Q5</spam>': q5,
    '<img class="layer_label" src="https://edpp.cz/public/images/new-edpp-viewer/icons/q20-icon.png" alt="q20-icon.png icon" height="16" width="16"><spam>Záplavové území Q20</spam>': q20,
    '<img class="layer_label" src="https://edpp.cz/public/images/new-edpp-viewer/icons/q100-icon.png" alt="q100-icon icon" height="16" width="16"><spam>Záplavové území Q100</spam>': q100,
    '<img class="layer_label" src="https://edpp.cz/public/images/new-edpp-viewer/icons/obce-icon.png" alt="obce-icon icon" height="16" width="16"><spam>Obce</spam>': obce,
    '<img class="layer_label" src="https://edpp.cz/public/images/new-edpp-viewer/icons/orp-icon.png" alt="orp-icon icon" height="16" width="16"><spam>ORP</spam>': orp,
    '<img class="layer_label" src="https://edpp.cz/public/images/new-edpp-viewer/icons/mestske-casti-icon.png" alt="mestske-cati icon" height="16" width="16"><spam>Městské části</spam>': mestcasti,
};

function getAddress() {
    var url = window.location.href,
        lat,
        lon,
        zoom;
    if (url.indexOf('?address=') !== -1) {
        url = url.replace('#', '').replace(' ', '%20');
        lat = url.split('?address=')[1].split('%20')[0];
        lon = url.split('?address=')[1].split('%20')[1];
        zoom = '15';
    } else {
        lat = 49.1927;https://www.edpp.cz/
        lon = 16.6031;
        zoom = 8;
    }
    return [lat, lon, zoom]
};

var start_layers = [mapy_cz, hladinomery, srazkomery];

function removeAccent(strAccents) {
    var strAccents = strAccents.split('');
    var strAccentsOut = new Array();
    var strAccentsLen = strAccents.length;
    var accents = 'áäčďéěíĺľňóô öŕšťúů üýřžÁÄČĎÉĚÍĹĽŇÓÔ ÖŔŠŤÚŮ ÜÝŘŽ';
    var accentsOut = "aacdeeillnoo orstuu uyrzAACDEEILLNOO ORSTUU UYRZ";
    for (var y = 0; y < strAccentsLen; y++) {
        if (accents.indexOf(strAccents[y]) != -1) {
            strAccentsOut[y] = accentsOut.substr(accents.indexOf(strAccents[y]), 1);
        } else
            strAccentsOut[y] = strAccents[y];
    }
    strAccentsOut = strAccentsOut.join('');
    return strAccentsOut;
};

function getPlan() {
    var url = window.location.href,
        name;
    if (url.indexOf('?q=') !== -1) {
        name = url.split('?q=')[1].split('#map_border')[0];
        if (name) {
            name = name;
            name_name = "'%" + decodeURIComponent(name).replace('+', ' ') + "%'";
            url_name = "'%" + removeAccent(decodeURIComponent(name)).replace('+', '-') + "%'";
            var query = 'name ILIKE ' + name_name + ' OR url ILIKE ' + url_name;
            console.log(query);
            hladinomery.wmsParams.CQL_FILTER = query;
            srazkomery.wmsParams.CQL_FILTER = query;
            obce.wmsParams.CQL_FILTER = query;
            orp.wmsParams.CQL_FILTER = query;
            mestcasti.wmsParams.CQL_FILTER = query;
            //start_layers.push(orp, obce, mestcasti);
            start_layers.push(obce, mestcasti, orp);
            selectparams.CQL_FILTER = query;

        } else {
            return false
        }

    } else {
        return false
    }
}
getPlan();
var map = L.map('map', {
    center: [getAddress()[0], getAddress()[1]],
    zoom: getAddress()[2],
    maxBounds: [
        [47, 11],
        [52, 20]
    ],
    minZoom: 6,
    layers: start_layers,
    fullscreenControl: true,
});

var baseLayers = {
    "ENVI OSM základní": mapy_cz,
    "ENVI OSM šedá": grayscale,
    "Ortofotomapa": ortofoto,
};

L.control.scale({ metric: true, imperial: false }).addTo(map);

L.Control.Locate = L.Control.extend({
    options: {
        position: 'topleft',
        drawCircle: true,
        follow: false, // follow with zoom and pan the user's location
        stopFollowingOnDrag: false, // if follow is true, stop following when map is dragged (deprecated)
        // if true locate control remains active on click even if the user's location is in view.
        // clicking control will just pan to location
        remainActive: false,
        markerClass: L.circleMarker, // L.circleMarker or L.marker
        // range circle
        circleStyle: {
            color: '#136AEC',
            fillColor: '#136AEC',
            fillOpacity: 0.15,
            weight: 2,
            opacity: 0.5
        },
        // inner marker
        markerStyle: {
            color: '#136AEC',
            fillColor: '#2A93EE',
            fillOpacity: 0.7,
            weight: 2,
            opacity: 0.9,
            radius: 5
        },
        // changes to range circle and inner marker while following
        // it is only necessary to provide the things that should change
        followCircleStyle: {},
        followMarkerStyle: {
            //color: '#FFA500',
            //fillColor: '#FFB000'
        },
        icon: 'fa fa-map-marker', // icon-location or icon-direction
        iconLoading: 'fa fa-spinner fa-spin',
        circlePadding: [0, 0],
        metric: true,
        onLocationError: function(err) {
            // this event is called in case of any location error
            // that is not a time out error.
            alert(err.message);
        },
        onLocationOutsideMapBounds: function(control) {
            // this event is repeatedly called when the location changes
            control.stopLocate();
            alert(control.options.strings.outsideMapBoundsMsg);
        },
        setView: true, // automatically sets the map view to the user's location
        // keep the current map zoom level when displaying the user's location. (if 'false', use maxZoom)
        keepCurrentZoomLevel: false,
        showPopup: false, // display a popup when the user click on the inner marker
        strings: {
            title: "Hledej ...",
            popup: "Jsi {distance} {unit} od tohoto místa",
            outsideMapBoundsMsg: "Jsi mimo mapu :-D"
        },
        locateOptions: {
            maxZoom: Infinity,
            watch: true // if you overwrite this, visualization cannot be updated
        }
    },

    onAdd: function(map) {
        var container = L.DomUtil.create('div',
            'leaflet-control-locate leaflet-bar leaflet-control');

        var self = this;
        this._layer = new L.LayerGroup();
        this._layer.addTo(map);
        this._event = undefined;

        this._locateOptions = this.options.locateOptions;
        L.extend(this._locateOptions, this.options.locateOptions);
        L.extend(this._locateOptions, {
            setView: false // have to set this to false because we have to
                // do setView manually
        });

        // extend the follow marker style and circle from the normal style
        var tmp = {};
        L.extend(tmp, this.options.markerStyle, this.options.followMarkerStyle);
        this.options.followMarkerStyle = tmp;
        tmp = {};
        L.extend(tmp, this.options.circleStyle, this.options.followCircleStyle);
        this.options.followCircleStyle = tmp;

        var link = L.DomUtil.create('a', 'leaflet-bar-part leaflet-bar-part-single ' + this.options.icon, container);
        link.href = '#';
        link.title = this.options.strings.title;

        L.DomEvent
            .on(link, 'click', L.DomEvent.stopPropagation)
            .on(link, 'click', L.DomEvent.preventDefault)
            .on(link, 'click', function() {
                var shouldStop = (self._event === undefined || map.getBounds().contains(self._event.latlng) || !self.options.setView || isOutsideMapBounds())
                if (!self.options.remainActive && (self._active && shouldStop)) {
                    stopLocate();
                } else {
                    locate();
                }
            })
            .on(link, 'dblclick', L.DomEvent.stopPropagation);

        var locate = function() {
            if (self.options.setView) {
                self._locateOnNextLocationFound = true;
            }
            if (!self._active) {
                map.locate(self._locateOptions);
            }
            self._active = true;
            if (self.options.follow) {
                startFollowing();
            }
            if (!self._event) {
                setClasses('requesting');
            } else {
                visualizeLocation();
            }
        };

        var onLocationFound = function(e) {
            // no need to do anything if the location has not changed
            if (self._event &&
                (self._event.latlng.lat === e.latlng.lat &&
                    self._event.latlng.lng === e.latlng.lng &&
                    self._event.accuracy === e.accuracy)) {
                return;
            }

            if (!self._active) {
                return;
            }

            self._event = e;

            if (self.options.follow && self._following) {
                self._locateOnNextLocationFound = true;
            }

            visualizeLocation();
        };

        var startFollowing = function() {
            map.fire('startfollowing', self);
            self._following = true;
            if (self.options.stopFollowingOnDrag) {
                map.on('dragstart', stopFollowing);
            }
        };

        var stopFollowing = function() {
            map.fire('stopfollowing', self);
            self._following = false;
            if (self.options.stopFollowingOnDrag) {
                map.off('dragstart', stopFollowing);
            }
            setContainerStyle();
        };

        var isOutsideMapBounds = function() {
            if (self._event === undefined)
                return false;
            return map.options.maxBounds &&
                !map.options.maxBounds.contains(self._event.latlng);
        };

        var visualizeLocation = function() {
            if (self._event.accuracy === undefined)
                self._event.accuracy = 0;

            var radius = self._event.accuracy;
            if (self._locateOnNextLocationFound) {
                if (isOutsideMapBounds()) {
                    self.options.onLocationOutsideMapBounds(self);
                } else {
                    map.fitBounds(self._event.bounds, {
                        padding: self.options.circlePadding,
                        maxZoom: self.options.keepCurrentZoomLevel ? map.getZoom() : self._locateOptions.maxZoom
                    });
                }
                self._locateOnNextLocationFound = false;
            }

            // circle with the radius of the location's accuracy
            var style, o;
            if (self.options.drawCircle) {
                if (self._following) {
                    style = self.options.followCircleStyle;
                } else {
                    style = self.options.circleStyle;
                }

                if (!self._circle) {
                    self._circle = L.circle(self._event.latlng, radius, style)
                        .addTo(self._layer);
                } else {
                    self._circle.setLatLng(self._event.latlng).setRadius(radius);
                    for (o in style) {
                        self._circle.options[o] = style[o];
                    }
                }
            }

            var distance, unit;
            if (self.options.metric) {
                distance = radius.toFixed(0);
                unit = "meters";
            } else {
                distance = (radius * 3.2808399).toFixed(0);
                unit = "feet";
            }

            // small inner marker
            var mStyle;
            if (self._following) {
                mStyle = self.options.followMarkerStyle;
            } else {
                mStyle = self.options.markerStyle;
            }

            if (!self._marker) {
                self._marker = self.options.markerClass(self._event.latlng, mStyle)
                    .addTo(self._layer);
            } else {
                self._marker.setLatLng(self._event.latlng);
                for (o in mStyle) {
                    self._marker.options[o] = mStyle[o];
                }
            }

            var t = self.options.strings.popup;
            if (self.options.showPopup && t) {
                self._marker.bindPopup(L.Util.template(t, {
                        distance: distance,
                        unit: unit
                    }))
                    ._popup.setLatLng(self._event.latlng);
            }

            setContainerStyle();
        };

        var setContainerStyle = function() {
            if (!self._container)
                return;
            if (self._following) {
                setClasses('following');
            } else {
                setClasses('active');
            }
        };

        var setClasses = function(state) {
            if (state == 'requesting') {
                L.DomUtil.removeClasses(self._container, "active following");
                L.DomUtil.addClasses(self._container, "requesting");

                L.DomUtil.removeClasses(link, self.options.icon);
                L.DomUtil.addClasses(link, self.options.iconLoading);
            } else if (state == 'active') {
                L.DomUtil.removeClasses(self._container, "requesting following");
                L.DomUtil.addClasses(self._container, "active");

                L.DomUtil.removeClasses(link, self.options.iconLoading);
                L.DomUtil.addClasses(link, self.options.icon);
            } else if (state == 'following') {
                L.DomUtil.removeClasses(self._container, "requesting");
                L.DomUtil.addClasses(self._container, "active following");

                L.DomUtil.removeClasses(link, self.options.iconLoading);
                L.DomUtil.addClasses(link, self.options.icon);
            }
        };

        var resetVariables = function() {
            self._active = false;
            self._locateOnNextLocationFound = self.options.setView;
            self._following = false;
        };

        resetVariables();

        var stopLocate = function() {
            map.stopLocate();
            map.off('dragstart', stopFollowing);
            if (self.options.follow && self._following) {
                stopFollowing();
            }

            L.DomUtil.removeClass(self._container, "requesting");
            L.DomUtil.removeClass(self._container, "active");
            L.DomUtil.removeClass(self._container, "following");
            resetVariables();

            self._layer.clearLayers();
            self._marker = undefined;
            self._circle = undefined;
        };

        var onLocationError = function(err) {
            // ignore time out error if the location is watched
            if (err.code == 3 && self._locateOptions.watch) {
                return;
            }

            stopLocate();
            self.options.onLocationError(err);
        };

        // event hooks
        map.on('locationfound', onLocationFound, self);
        map.on('locationerror', onLocationError, self);

        // make locate functions available to outside world
        this.locate = locate;
        this.stopLocate = stopLocate;
        this.stopFollowing = stopFollowing;

        return container;
    }
});

L.Map.addInitHook(function() {
    if (this.options.locateControl) {
        this.locateControl = L.control.locate();
        this.addControl(this.locateControl);
    }
});

L.control.locate = function(options) {
    return new L.Control.Locate({
        locateOptions: {
            enableHighAccuracy: true
        }
    });
};

(function() {
    // leaflet.js raises bug when trying to addClass / removeClass multiple classes at once
    // Let's create a wrapper on it which fixes it.
    var LDomUtilApplyClassesMethod = function(method, element, classNames) {
        classNames = classNames.split(' ');
        classNames.forEach(function(className) {
            L.DomUtil[method].call(this, element, className);
        });
    };

    L.DomUtil.addClasses = function(el, names) {
        LDomUtilApplyClassesMethod('addClass', el, names);
    };
    L.DomUtil.removeClasses = function(el, names) {
        LDomUtilApplyClassesMethod('removeClass', el, names);
    };
})();


L.Control.Geocoder = L.Control.extend({
    options: {
        showResultIcons: false,
        collapsed: true,
        expand: 'click',
        position: 'topright',
        placeholder: 'Vyhledej...',
        errorMessage: 'Místo nenalezeno'
    },

    _callbackId: 0,

    initialize: function(options) {
        L.Util.setOptions(this, options);
        if (!this.options.geocoder) {
            this.options.geocoder = new L.Control.Geocoder.Nominatim();
        }
    },

    onAdd: function(map) {
        var className = 'leaflet-control-geocoder',
            container = L.DomUtil.create('div', className + ' leaflet-bar'),
            icon = L.DomUtil.create('a', 'leaflet-control-geocoder-icon', container),
            form = this._form = L.DomUtil.create('form', className + '-form', container),
            input;

        icon.innerHTML = '&nbsp;';
        icon.href = 'javascript:void(0);';
        this._map = map;
        this._container = container;
        input = this._input = L.DomUtil.create('input');
        input.type = 'text';
        input.placeholder = this.options.placeholder;

        L.DomEvent.addListener(input, 'keydown', this._keydown, this);
        //L.DomEvent.addListener(input, 'onpaste', this._clearResults, this);
        //L.DomEvent.addListener(input, 'oninput', this._clearResults, this);

        this._errorElement = document.createElement('div');
        this._errorElement.className = className + '-form-no-error';
        this._errorElement.innerHTML = this.options.errorMessage;

        this._alts = L.DomUtil.create('ul', className + '-alternatives leaflet-control-geocoder-alternatives-minimized');

        form.appendChild(input);
        this._container.appendChild(this._errorElement);
        container.appendChild(this._alts);

        L.DomEvent.addListener(form, 'submit', this._geocode, this);

        if (this.options.collapsed) {
            if (this.options.expand === 'click') {
                L.DomEvent.addListener(icon, 'click', function(e) {
                    // TODO: touch
                    if (e.button === 0 && e.detail !== 2) {
                        this._toggle();
                    }
                }, this);
            } else {
                L.DomEvent.addListener(icon, 'mouseover', this._expand, this);
                L.DomEvent.addListener(icon, 'mouseout', this._collapse, this);
                this._map.on('movestart', this._collapse, this);
            }
        } else {
            L.DomEvent.addListener(icon, 'click', function(e) {
                this._geocode(e);
            }, this);
            this._expand();
        }

        L.DomEvent.disableClickPropagation(container);

        return container;
    },

    _geocodeResult: function(results) {
        L.DomUtil.removeClass(this._container, 'leaflet-control-geocoder-throbber');
        if (results.length === 1) {
            this._geocodeResultSelected(results[0]);
        } else if (results.length > 0) {
            this._alts.innerHTML = '';
            this._results = results;
            L.DomUtil.removeClass(this._alts, 'leaflet-control-geocoder-alternatives-minimized');
            for (var i = 0; i < results.length; i++) {
                this._alts.appendChild(this._createAlt(results[i], i));
            }
        } else {
            L.DomUtil.addClass(this._errorElement, 'leaflet-control-geocoder-error');
        }
    },

    markGeocode: function(result) {
        this._map.fitBounds(result.bbox);

        if (this._geocodeMarker) {
            this._map.removeLayer(this._geocodeMarker);
        }

        this._geocodeMarker = new L.Marker(result.center)
            .bindPopup(result.html || result.name)
            .addTo(this._map)
            .openPopup();

        return this;
    },

    _geocode: function(event) {
        L.DomEvent.preventDefault(event);

        L.DomUtil.addClass(this._container, 'leaflet-control-geocoder-throbber');
        this._clearResults();
        this.options.geocoder.geocode(this._input.value, this._geocodeResult, this);

        return false;
    },

    _geocodeResultSelected: function(result) {
        if (this.options.collapsed) {
            this._collapse();
        } else {
            this._clearResults();
        }
        this.markGeocode(result);
    },

    _toggle: function() {
        if (this._container.className.indexOf('leaflet-control-geocoder-expanded') >= 0) {
            this._collapse();
        } else {
            this._expand();
        }
    },

    _expand: function() {
        L.DomUtil.addClass(this._container, 'leaflet-control-geocoder-expanded');
        this._input.select();
    },

    _collapse: function() {
        this._container.className = this._container.className.replace(' leaflet-control-geocoder-expanded', '');
        L.DomUtil.addClass(this._alts, 'leaflet-control-geocoder-alternatives-minimized');
        L.DomUtil.removeClass(this._errorElement, 'leaflet-control-geocoder-error');
    },

    _clearResults: function() {
        L.DomUtil.addClass(this._alts, 'leaflet-control-geocoder-alternatives-minimized');
        this._selection = null;
        L.DomUtil.removeClass(this._errorElement, 'leaflet-control-geocoder-error');
    },

    _createAlt: function(result, index) {
        var li = L.DomUtil.create('li', ''),
            a = L.DomUtil.create('a', '', li),
            icon = this.options.showResultIcons && result.icon ? L.DomUtil.create('img', '', a) : null,
            text = result.html ? undefined : document.createTextNode(result.name),
            clickHandler = function clickHandler(e) {
                L.DomEvent.preventDefault(e);
                this._geocodeResultSelected(result);
            };

        if (icon) {
            icon.src = result.icon;
        }

        li.setAttribute('data-result-index', index);

        if (result.html) {
            a.innerHTML = result.html;
        } else {
            a.appendChild(text);
        }

        L.DomEvent.addListener(li, 'click', clickHandler, this);

        return li;
    },

    _keydown: function(e) {
        var _this = this,
            select = function select(dir) {
                if (_this._selection) {
                    L.DomUtil.removeClass(_this._selection, 'leaflet-control-geocoder-selected');
                    _this._selection = _this._selection[dir > 0 ? 'nextSibling' : 'previousSibling'];
                }
                if (!_this._selection) {
                    _this._selection = _this._alts[dir > 0 ? 'firstChild' : 'lastChild'];
                }

                if (_this._selection) {
                    L.DomUtil.addClass(_this._selection, 'leaflet-control-geocoder-selected');
                }
            };

        switch (e.keyCode) {
            // Escape
            case 27:
                if (this.options.collapsed) {
                    this._collapse();
                }
                break;
                // Up
            case 38:
                select(-1);
                L.DomEvent.preventDefault(e);
                break;
                // Up
            case 40:
                select(1);
                L.DomEvent.preventDefault(e);
                break;
                // Enter
            case 13:
                if (this._selection) {
                    var index = parseInt(this._selection.getAttribute('data-result-index'), 10);
                    this._geocodeResultSelected(this._results[index]);
                    this._clearResults();
                    L.DomEvent.preventDefault(e);
                }
        }
        return true;
    }
});

L.Control.geocoder = function(options) {
    return new L.Control.Geocoder(options);
};

L.Control.Geocoder.callbackId = 0;
L.Control.Geocoder.jsonp = function(url, params, callback, context, jsonpParam) {
    var callbackId = '_l_geocoder_' + (L.Control.Geocoder.callbackId++);
    params[jsonpParam || 'callback'] = callbackId;
    window[callbackId] = L.Util.bind(callback, context);
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url + L.Util.getParamString(params);
    script.id = callbackId;
    document.getElementsByTagName('head')[0].appendChild(script);
};
L.Control.Geocoder.getJSON = function(url, params, callback) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState != 4) {
            return;
        }
        if (xmlHttp.status != 200 && xmlHttp.status != 304) {
            callback('');
            return;
        }
        callback(JSON.parse(xmlHttp.response));
    };
    xmlHttp.open("GET", url + L.Util.getParamString(params), true);
    xmlHttp.setRequestHeader("Accept", "application/json");
    xmlHttp.send(null);
};

L.Control.Geocoder.template = function(str, data, htmlEscape) {
    return str.replace(/\{ *([\w_]+) *\}/g, function(str, key) {
        var value = data[key];
        if (value === undefined) {
            value = '';
        } else if (typeof value === 'function') {
            value = value(data);
        }
        return L.Control.Geocoder.htmlEscape(value);
    });
};

// Adapted from handlebars.js
// https://github.com/wycats/handlebars.js/
L.Control.Geocoder.htmlEscape = (function() {
    var badChars = /[&<>"'`]/g;
    var possible = /[&<>"'`]/;
    var escape = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        '\'': '&#x27;',
        '`': '&#x60;'
    };

    function escapeChar(chr) {
        return escape[chr];
    }

    return function(string) {
        if (string == null) {
            return '';
        } else if (!string) {
            return string + '';
        }

        // Force a string conversion as this will be done by the append regardless and
        // the regex test will do this transparently behind the scenes, causing issues if
        // an object's to string has escaped characters in it.
        string = '' + string;

        if (!possible.test(string)) {
            return string;
        }
        return string.replace(badChars, escapeChar);
    };
})();


L.Control.Geocoder.Nominatim = L.Class.extend({
    options: {
        serviceUrl: '//nominatim.openstreetmap.org/',
        geocodingQueryParams: {},
        reverseQueryParams: {},
        htmlTemplate: function(r) {
            var a = r.address,
                parts = [];
            if (a.road || a.building) {
                parts.push('{building} {road} {house_number}');
            }

            if (a.city || a.town || a.village) {
                parts.push('<span class="' + (parts.length > 0 ? 'leaflet-control-geocoder-address-detail' : '') +
                    '">{postcode} {city} {town} {village}</span>');
            }

            if (a.state || a.country) {
                parts.push('<span class="' + (parts.length > 0 ? 'leaflet-control-geocoder-address-context' : '') +
                    '">{state} {country}</span>');
            }

            return L.Control.Geocoder.template(parts.join('<br/>'), a, true);
        }
    },

    initialize: function(options) {
        L.Util.setOptions(this, options);
    },

    geocode: function(query, cb, context) {
        L.Control.Geocoder.jsonp(this.options.serviceUrl + 'search/', L.extend({
                q: query,
                limit: 5,
                format: 'json',
                addressdetails: 1
            }, this.options.geocodingQueryParams),
            function(data) {
                var results = [];
                for (var i = data.length - 1; i >= 0; i--) {
                    var bbox = data[i].boundingbox;
                    for (var j = 0; j < 4; j++) bbox[j] = parseFloat(bbox[j]);
                    results[i] = {
                        icon: data[i].icon,
                        name: data[i].display_name,
                        html: this.options.htmlTemplate ?
                            this.options.htmlTemplate(data[i]) : undefined,
                        bbox: L.latLngBounds([bbox[0], bbox[2]], [bbox[1], bbox[3]]),
                        center: L.latLng(data[i].lat, data[i].lon),
                        properties: data[i]
                    };
                }
                cb.call(context, results);
            }, this, 'json_callback');
    },

    reverse: function(location, scale, cb, context) {
        L.Control.Geocoder.jsonp(this.options.serviceUrl + 'reverse/', L.extend({
            lat: location.lat,
            lon: location.lng,
            zoom: Math.round(Math.log(scale / 256) / Math.log(2)),
            addressdetails: 1,
            format: 'json'
        }, this.options.reverseQueryParams), function(data) {
            var result = [],
                loc;

            if (data && data.lat && data.lon) {
                loc = L.latLng(data.lat, data.lon);
                result.push({
                    name: data.display_name,
                    html: this.options.htmlTemplate ?
                        this.options.htmlTemplate(data) : undefined,
                    center: loc,
                    bounds: L.latLngBounds(loc, loc),
                    properties: data
                });
            }

            cb.call(context, result);
        }, this, 'json_callback');
    }
});

L.Control.Geocoder.nominatim = function(options) {
    return new L.Control.Geocoder.Nominatim(options);
};

L.Control.Radar = L.Control.extend({
    options: {
        position: 'topright',
        title: 'Srážkový radar',
        bounds: null,
        visible: false
    },

    initialize: function(options) {
        L.Control.prototype.initialize.call(this, options);
        this._radarImages = this._loadRadarImages();
        //this._image;
        //this._map;
        this._interval = 0;
        this._radarIndex = 0;
    },
    onAdd: function(map) {
        this._map = map;
        var className = 'leaflet-control-radar',
            that = this,
            container = this._container = L.DomUtil.create('div', className);

        L.DomEvent.disableClickPropagation(container);

        this._createButton(this.options.title, className, container, map);
        this._createTime(className, container, this.options.visible);

        /* If image should be visible, add it to the map */
        if (this.options.visible) {
            var imageUrl = this._radarImages[0].src;
            this._image = L.imageOverlay(imageUrl, this.options.bounds);

            this._image.addTo(this._map);
            this._image.setOpacity(0.4);
            this._interval = setInterval(function() {
                that._animateRadar();
            }, 1000);
        }

        return container;
    },
    toggleRadar: function() {
        var that = this;

        if (this.options.visible) {
            this._image.setOpacity(0);
            clearTimeout(this._interval);
        } else {
            this._interval = setInterval(function() {
                that._animateRadar();
            }, 1000);
        }

        L.DomUtil.get(document.getElementsByClassName('leaflet-control-radar-time')[0]).style.display = !this.options.visible ? 'inline-block' : 'none'
        this.options.visible = !this.options.visible;
    },

    _animateRadar: function() {
        var url = this._radarImages[this._radarIndex].src;
        if (typeof this._image !== 'undefined') {
            this._image.setUrl(url);
        } else {
            this._image = L.imageOverlay(url, this.options.bounds);
            this._image.addTo(this._map);
        }
        this._image.setOpacity(0.4);

        this._radarIndex += 1;

        if (this._radarIndex === this._radarImages.length) {
            this._radarIndex = 0;
        }
        var time = L.DomUtil.get(document.getElementsByClassName('leaflet-control-radar-time')[0]);
        time.innerHTML = this._formatTime(url);
    },
    _formatTime: function(timestring) {
        var temp = timestring.split('.').slice(6, 8),
            day = temp[0].slice(-2),
            month = temp[0].slice(-4, -2),
            hour = parseInt(temp[1].slice(-4, -2)),
            minute = temp[1].slice(-2);

        switch (hour) {
            case 22:
                hour = 0;
                break;
            case 23:
                hour = 1;
                break;
            default:
                hour += 2;
        }

        var time = [day + '.', month + '.', '<br>', hour + ':', minute];

        for (var i = time.length - 1; i >= 0; i -= 1) {
            if (time[i].indexOf('0') === 0 && time[i].substring(0, 1) == '00') {
                time[i] = time[i].slice(1);
            }
        }

        return time.join('')
    },
    _createButton: function(title, className, container, context) {
        var link = L.DomUtil.create('a', className + '-toggle', container);
        link.href = '#';
        link.title = this.options.title;

        L.DomEvent.addListener(link, 'click', this.toggleRadar, this);

        return link;
    },

    /**
     * Create span container for time and date of an image.
     * @param {string} className
     * @param {HTMLElement} container
     * @param {boolean} display should be the span visible
     */
    _createTime: function(className, container, display) {
        var text = L.DomUtil.create('span', className + '-time', container);
        text.style.display = display ? 'inline-block' : 'none';
        return text;
    },
    _loadRadarImages: function() {
        var time = new Date(),
            year = time.getFullYear(),
            month = time.getMonth() /* 0-11 */ ,
            day = time.getUTCDate() /* 1-31 */ ,
            hour = time.getUTCHours() // - Math.abs(time.getTimezoneOffset() / 60)  /* 0-23 */
            ,
            minute = time.getMinutes(),
            radarImages = [],
            i = 0,
            tempMonth = new String(month + 1).length === 1 ? "0" + (month + 1) : month + 1,
            tempDay = new String(day + 1).length === 1 ? "0" + (day) : day,
            quarter;

        if (minute < 15) {
            quarter = 0;
        } else if (minute < 30) {
            quarter = 15;
        } else if (minute < 45) {
            quarter = 30;
        } else {
            quarter = 45;
        }

        var tempQuarter = quarter.toString().length === 1 ? "0" + quarter : quarter,
            url = 'https://edpp.cz/public/proxy.php?url=http://radar.bourky.cz/data/pacz2gmaps.z_max3d.' + year + tempMonth + tempDay + '.' + hour + tempQuarter + '.0.png';

        for (var i; i < 15; i += 1) {
            if (quarter === 0) {
                quarter = 45;
                hour = (hour - 1) < 0 ? 23 : hour - 1;
            } else {
                quarter -= 15;
            }

            if (hour === 23 && quarter === 45) {
                if (!(day - 1 <= 0)) {
                    day -= 1;
                } else {
                    month = month === 0 ? 11 : month - 1;
                    day = time.monthDays(year, month);
                }
            }

            if (month === 11 && day === 31 && hour === 23 && quarter === 45) {
                year -= 1;
            }
            var tempMonth = (month + 1).toString().length === 1 ? "0" + (month + 1) : month + 1,
                tempDay = day.toString().length === 1 ? "0" + day : day,
                tempHour = hour.toString().length === 1 ? "0" + hour : hour,
                tempQuarter = quarter.toString().length === 1 ? "0" + quarter : quarter,
                newURL = 'https://edpp.cz/public/proxy.php?url=http://radar.bourky.cz/data/pacz2gmaps.z_max3d.' + year + tempMonth + tempDay + '.' + tempHour + tempQuarter + '.0.png',
                img = new Image();

            img.src = newURL;
            radarImages.unshift(img);
        }
        return radarImages;
    }
});
var radar = new L.Control.Radar({
    position: 'topright',
    bounds: L.latLngBounds(L.latLng(51.87092, 10.07526), L.latLng(47.09390, 20.22663)),
    visible: false
});

L.Control.Coordinates = L.Control.extend({
    options: {
        position: 'bottomleft',
        latitudeText: 'z. š.',
        longitudeText: 'z. d.',
        promptText: 'Stiskněte Ctrl+C pro zkopírování souřadnic',
        precision: 4
    },

    initialize: function(options) {
        L.Control.prototype.initialize.call(this, options);
    },

    onAdd: function(map) {
        var className = 'leaflet-control-coordinates',
            that = this,
            container = this._container = L.DomUtil.create('div', className);

        L.DomEvent.disableClickPropagation(container);

        this._addText(container, map);

        L.DomEvent.addListener(container, 'click', function() {
            var lat = L.DomUtil.get(that._lat),
                lng = L.DomUtil.get(that._lng),
                latTextLen = this.options.latitudeText.length + 1,
                lngTextLen = this.options.longitudeText.length + 1,
                latTextIndex = lat.textContent.indexOf(this.options.latitudeText) + latTextLen,
                lngTextIndex = lng.textContent.indexOf(this.options.longitudeText) + lngTextLen,
                latCoordinate = lat.textContent.substr(latTextIndex),
                lngCoordinate = lng.textContent.substr(lngTextIndex);

            window.prompt(this.options.promptText, latCoordinate + ' ' + lngCoordinate);
        }, this);

        return container;
    },



    _addText: function(container, context) {
        this._lat = L.DomUtil.create('span', 'leaflet-control-coordinates-lat', container),
            this._lng = L.DomUtil.create('span', 'leaflet-control-coordinates-lng', container);

        return container;
    },

    /**
     * @param event object
     */
    setCoordinates: function(obj) {
        if (obj.latlng) {
            L.DomUtil.get(this._lat).innerHTML = '<strong>' + this.options.latitudeText + ':</strong>' + obj.latlng.lat.toFixed(this.options.precision).toString();
            L.DomUtil.get(this._lng).innerHTML = '<strong>' + this.options.longitudeText + ':</strong>' + obj.latlng.lng.toFixed(this.options.precision).toString();
        }
    }
});

this.coordinates = new L.Control.Coordinates({
    position: 'bottomleft'
});

map.on('click', function(e) {
    if (!coordinatesVisible) {
        map.addControl(coordinates);
    }

    coordinates.setCoordinates(e);
    var coordinatesVisible = true;
});


L.control.layers(baseLayers, layers).addTo(map);
orp.bringToFront();
obce.bringToFront();
mestcasti.bringToFront();
hladinomery.bringToFront();
srazkomery.bringToFront();
L.control.locate().addTo(map);
L.Control.geocoder().addTo(map);
map.addControl(this.radar);
//map.addControl(new L.Control.Fullscreen());